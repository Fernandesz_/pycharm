from django.urls import path
from app.views import create, list

urlpatterns = [
    path('', list, name="list"),
    path('create/', create, name="create"),
]